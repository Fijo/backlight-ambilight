int incomingByte = 0;
int ledNr = 0;
int nextBit = 0;
int lastByte = 0;
byte leds[4] = {9, 10, 11, 6};
int loopCounter = 0;

void setup() {
  Serial.begin(9600); 
}

void loop() {
  if(Serial.available() != 0)  {
    incomingByte = Serial.read();
    if(incomingByte != lastByte)  {
      lastByte = incomingByte;

      boolean isLedNr = (incomingByte & 128) == 128;
      if(isLedNr)  {
        ledNr = incomingByte & 63;
        nextBit = (incomingByte & 64) << 1;
      }
      else  {  
        int fadeValue = nextBit + incomingByte;
        analogWrite(leds[ledNr], fadeValue);
      }

      Serial.print("!");
    }
  }
}

