package Backlight;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ComWriter  {
	private InputStream _input;
	private OutputStream _output;
	private int _maxTrys;
	private int _availble;
	private boolean _reCheckAvailibleBeforeWrite;
	private boolean _sendOnlyChanges;
	
	public ComWriter(InputStream input, OutputStream output, int maxTrys, boolean sendOnlyChanges, boolean reCheckAvailibleBeforeWrite, boolean checkAvailibleBeforeUsage)	{
		_input = input;
		_output = output;
		_maxTrys = maxTrys;
		_reCheckAvailibleBeforeWrite = reCheckAvailibleBeforeWrite;
		_sendOnlyChanges = sendOnlyChanges;
		
		Init(!reCheckAvailibleBeforeWrite && checkAvailibleBeforeUsage);
	}
	
	private void Init(boolean checkAvailibleBeforeUsage)	{
		if(checkAvailibleBeforeUsage)
			try {
				_availble = _input.available();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public void DimLed(Led led, int value) throws IOException, InterruptedException	{
		if(!_sendOnlyChanges || value != led.CurrentState)	{
			DimLedId(led.LedNr, value);
			led.CurrentState = (byte) value;
		}
	}
		
	private void DimLedId(int ledNr, int value) throws IOException, InterruptedException	{
		WriteByte((byte) ((ledNr & 63) + ((value & 128) == 128 ? 64 : 0) + 128));
		WriteByte((byte)(value & 127));
	}
	
	private void WriteByte(byte value) throws IOException, InterruptedException	{
		int count = GetCurrentAvailable();
		int i = 0;
		int avalible = 0;
		do	{
			_output.write(value);
		}
		while((avalible = _input.available()) == count || i++ < _maxTrys);
		_input.skip(avalible);
	}
	
	private int GetCurrentAvailable() throws IOException	{
		return _reCheckAvailibleBeforeWrite ? _input.available() : _availble;
	}
}