package Backlight;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;

public class ScreenShotPrefetchPix implements IScreenShotPix {
	private Robot _robot;
	private Rectangle _usedArea;
	private BufferedImage _cache;
	
	public ScreenShotPrefetchPix() throws AWTException{
		_robot = new Robot();
	}
	
	public void Init(Rectangle usedArea){
		_usedArea = usedArea;
	}

	public int GetRGB(int x, int y) {
		return _cache.getRGB(x, y);
	}

	public void Refresh() {
		_cache = _robot.createScreenCapture(_usedArea);		
	}

	public void RefreshPart(Rectangle usedArea, Rectangle[] lines) { }
}
