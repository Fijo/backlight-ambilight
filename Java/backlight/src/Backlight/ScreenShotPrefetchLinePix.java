package Backlight;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;

public class ScreenShotPrefetchLinePix implements IScreenShotPix {
	private Robot _robot;
	private BufferedImage _cache;
	private int _lineIndex;
	private Rectangle[] _lines;
	private int _startX;
	private int _startY;
	
	public ScreenShotPrefetchLinePix() throws AWTException{
		_robot = new Robot();
	}
	
	public void Init(Rectangle usedArea) { }

	public int GetRGB(int x, int y) {
		if(x != _startX) RefreshLine(x);
		return _cache.getRGB(x - _startX, y - _startY);
	}

	public void Refresh() {	}

	public void RefreshPart(Rectangle usedArea, Rectangle[] lines) {
		_startY = lines[0].y;
		_lineIndex = 0;
		_lines = lines;
		_startX = -1;
	}
	
	private void RefreshLine(int x)	{
		_cache = _robot.createScreenCapture(_lines[_lineIndex++]);
		_startX = x;
	}
}
