package Backlight;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;

public class ScreenShotPrefetchPartPix implements IScreenShotPix {
	private Robot _robot;
	private Rectangle _usedArea;
	private BufferedImage _cache;
	
	public ScreenShotPrefetchPartPix() throws AWTException{
		_robot = new Robot();
	}
	
	public void Init(Rectangle usedArea){
		_usedArea = usedArea;
	}

	public int GetRGB(int x, int y) {
		return _cache.getRGB(x, y);
	}

	public void Refresh() {	}

	public void RefreshPart(Rectangle usedArea, Rectangle[] lines) {
		_cache = _robot.createScreenCapture(_usedArea);
	}
}
