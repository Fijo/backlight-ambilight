// 32 Bit Java is needet for Linux http://debianforum.de/forum/viewtopic.php?f=27&t=94495
// Native Serial Lib is not 64 Bit compatible.

package Backlight;

public class Main {
	public static void main(String[] args) throws Exception {
		
		Backlight bl = new Backlight(GetConfiguration());
		bl.Start();
		bl.Close();
	}
	
	private static BacklightConfiguration GetConfiguration()	{
		int top = 100;
		int bottom = 100;
		int left = 0;
		int right = 0;
		int width = 1366;
		int height = 768;
		int pixAroundX = 350;
		int pixAroundY = 250;
		int skip = 24;
		
		BacklightConfiguration bc = new BacklightConfiguration();
		byte ledNr = 0;
		bc.Leds = new Led[]{
			new Led(ledNr++, left, top, pixAroundX, pixAroundY, skip, skip),
			new Led(ledNr++, width - right -1, top, -pixAroundX, pixAroundY, skip, skip),
			new Led(ledNr++, width - right -1, height - bottom -1, -pixAroundX, -pixAroundY, skip, skip),
			new Led(ledNr++, left, height - bottom -1, pixAroundX, -pixAroundY, skip, skip),
		};
		bc.WaitBetween = 20;
		bc.WaitBlock = 200;
		bc.ScreenFetchType = ScreenFetchType.PerRefresh;
		
		return bc;
	}
}