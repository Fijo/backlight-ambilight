package Backlight;

public class BacklightConfiguration {
	public Led[] Leds;
	public int WaitBetween;
	public int WaitBlock;
	public ScreenFetchType ScreenFetchType;
	public String PortName = "/dev/ttyACM0";
	public int SerialDataRate = 9600;
	public int SerialTimeOut = 2000;
	public int ConnectionInitSleep = 1500;
	public int PrintFpsFrame = 100;
	public boolean CheckAvailibleBeforeUsage = true;
	public boolean ReCheckAvailibleBeforeWrite = false;
	public boolean SendOnlyChanges = true;
	public int SendMaxTrys = 10;
}
