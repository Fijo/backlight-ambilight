package Backlight;

import java.awt.Rectangle;

public interface IScreenShotPix {
	void Init(Rectangle usedArea);
	void Refresh();
	int GetRGB(int x, int y);
	void RefreshPart(Rectangle usedArea, Rectangle[] lines);
}
