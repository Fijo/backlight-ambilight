package Backlight;

public enum ScreenFetchType {
	PerRefresh,
	PerLed,
	PerLine,
	PerPix
}
