package Backlight;

import java.awt.AWTException;

public class Backlight {
	private BacklightConfiguration _bc;
	private ArduinoCom _ac;
	
	public Backlight(BacklightConfiguration bc) throws AWTException	{
		_bc = bc;
		_ac = new ArduinoCom();
	}
	
	public void Start()	{
		_ac.Init(_bc);
	}
	
	public void Close()	{
		_ac.Close();
	}
}
