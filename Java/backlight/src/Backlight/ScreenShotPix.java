package Backlight;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;

public class ScreenShotPix implements IScreenShotPix {
	private Robot _robot;
	
	public ScreenShotPix() throws AWTException{
		_robot = new Robot();
	}

	public void Init(Rectangle usedArea) { }

	public int GetRGB(int x, int y) {
		return _robot.getPixelColor(x, y).getRGB();
	}
	
	public void Refresh() { }

	public void RefreshPart(Rectangle usedArea, Rectangle[] lines) { }
}
