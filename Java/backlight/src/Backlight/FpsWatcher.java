package Backlight;

import java.io.PrintStream;
import java.util.Date;

public class FpsWatcher {
	private PrintStream _stream;
	private String _format;
	private long _startTime;
	private int _frameNum;
	private int _printFrame;
	private boolean _enabled;
	
	public FpsWatcher(PrintStream stream, String format, int printFrame, boolean enabled)	{
		_stream = stream;
		_format = format;
		_printFrame = printFrame;
		_enabled = enabled;
	}

	public void Start()	{
		if(_enabled) _startTime = GetTime();
	}
	
	public void FrameEnd()	{
		if(_enabled && _frameNum++ % _printFrame == 0) WriteFps();
	}
	
	public void WriteFps()	{
		long runtime = GetTime() - _startTime;
		WriteToStream((_frameNum * 1000) / runtime);	
	}
	
	private void WriteToStream(float fps)	{
		_stream.format(_format, fps);
	}
	
	private long GetTime()	{
		return new Date().getTime();
	}

	public void SetEnabled(boolean enabled)	{
		_enabled = enabled;
	}
	
	public void Clear()	{
		_startTime = 0;
		_frameNum = 0;
	}
}
