package Backlight;

import java.awt.Rectangle;

public class Led {
	public int ScreenX;
	public int ScreenY;
	public int ScreenMX;
	public int ScreenMY;
	public int SkipX;
	public int SkipY;
	public LedScreenPixCachePoint[] PixCache;
	public float PixDivide;
	public byte LedNr;
	public byte CurrentState = 0;
	public Rectangle UsedArea;
	public Rectangle[] UsedAreaLines;
	
	public Led(byte ledNr, int screenX, int screenY, int screenMX, int screenMY, int skipX, int skipY)	{
		LedNr = ledNr;
		ScreenX = screenX;
		ScreenY = screenY;
		ScreenMX = screenMX;
		ScreenMY = screenMY;
		SkipX = skipX;
		SkipY = skipY;		
	}
}
