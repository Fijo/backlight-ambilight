package Backlight;

import java.io.InputStream;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import java.util.Enumeration;

public class ArduinoCom  {
	private SerialPort _serialPort;	
	private InputStream _input;
	private OutputStream _output;

	private CommPortIdentifier ResolvePort(BacklightConfiguration bc)	{
		CommPortIdentifier portId = null;
		@SuppressWarnings("rawtypes")
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
		String portName = bc.PortName;

		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			if (currPortId.getName() == portName) {
				portId = currPortId;
				break;
			}
		}

		if (portId == null) FailNoComPortFound();
		return portId;
	}
	
	private void FailNoComPortFound()	{
		System.err.println("Could not find COM port.");
		System.exit(1);
	}
	
	public void Init(BacklightConfiguration bc) {
		try {
			_serialPort = (SerialPort) (ResolvePort(bc).open(this.getClass().getName(), bc.SerialTimeOut));
			_serialPort.setSerialPortParams(bc.SerialDataRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			_input = _serialPort.getInputStream();
			_output = _serialPort.getOutputStream();
			
			Thread.sleep(bc.ConnectionInitSleep);

            new BacklightThread(bc, new ComWriter(_input, _output, bc.SendMaxTrys, bc.SendOnlyChanges, bc.ReCheckAvailibleBeforeWrite, bc.CheckAvailibleBeforeUsage)).Start();
		} catch (Exception e) {
			System.err.println(e.toString());
		}
    }
	
	public synchronized void Close() {
		if (_serialPort != null) _serialPort.close();
	}
}