package Backlight;

public class LedScreenPixCachePoint {
	public int X;
	public int Y;
	public float Intensity;
	
	public LedScreenPixCachePoint(int x, int y, float intensity)	{
		X = x;
		Y = y;
		Intensity = intensity;
	}
}
