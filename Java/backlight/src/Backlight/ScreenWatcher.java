package Backlight;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.util.Stack;

public class ScreenWatcher {
	private IScreenShotPix _ssp;
	private int _minX;
	private int _maxX;
	private int _minY;
	private int _maxY;
	
	public ScreenWatcher(IScreenShotPix ssp) throws AWTException{
		_ssp = ssp;
	}

	public void Refresh()	{
		_ssp.Refresh();
	}
	
	public void InitLedLight(Led led)	{
		int maxLightIntensity = 255 * 3;		
		float count = 0;
		Stack<LedScreenPixCachePoint> map = new Stack<LedScreenPixCachePoint>();
		
		int startY = led.ScreenY;
		
		int ix = led.ScreenX;
		int px = 1;
		int lx = ix + led.ScreenMX;
		int ly = startY + led.ScreenMY;
		int sx = led.SkipX;
		int sy = led.SkipY;
		int dx = GetCountDirection(led.ScreenMX, sx);
		int dy = GetCountDirection(led.ScreenMY, sy);
		
		int oldMinX = _minX;
		int oldMaxX = _maxX;
		int oldMinY = _minY;
		int oldMaxY = _maxY;
		ResetMinMax();
		
		UpdateUsedAreaX(ix);
		UpdateUsedAreaX(lx);
		UpdateUsedAreaY(startY);
		UpdateUsedAreaY(ly);

		int beginX = ix < lx ? ix : lx;
		int beginY = startY < ly ? startY : ly;
		int sizeY = (ly > startY ? ly : startY) - beginY +1;
		int sizeX = (lx > ix ? lx : ix) - beginX +1;
		
		int l = sizeX;
		Rectangle[] lines = new Rectangle[l / sx];
		
		for(int i = 0; ix != lx; ix += dx, px += sx, i++)	{
			lines[i] = new Rectangle(ix, beginY, 1, sizeY);
			float spaceAway = px;
			int iy = startY;
			int py = 1;
			for(; iy != ly; count += GetLightIntensity(maxLightIntensity, spaceAway), iy += dy, py += sy, spaceAway = px * py)	{
				map.add(new LedScreenPixCachePoint(ix, iy, GetLightIntensity(1, spaceAway)));
			}
		}

		led.PixCache = ConvertToArray(map);
		led.PixDivide = count / 255;
		led.UsedArea = GetRectangle();
		led.UsedAreaLines = lines;

		UpdateUsedAreaX(oldMaxX);
		UpdateUsedAreaX(oldMinX);
		UpdateUsedAreaY(oldMaxY);
		UpdateUsedAreaY(oldMinY);
	}
	
	private void ResetMinMax()	{
		_minX = 0;
		_maxX = 0;
		_minY = 0;
		_maxY = 0;
	}
	
	private Rectangle GetRectangle()	{
		return new Rectangle(_minX, _minY, _maxX - _minX +1, _maxY - _minY +1);
	}
	
	public void FinishInit()	{
		_ssp.Init(GetRectangle());
	}
	
	private void UpdateUsedAreaX(int x)	{
		if(x < _minX) _minX = x;
		if(x > _maxX) _maxX = x;
	}

	private void UpdateUsedAreaY(int y)	{
		if(y < _minY) _minY = y;
		if(y > _maxY) _maxY = y;		
	}
	
	public byte GetLedLight(Led led)	{
		_ssp.RefreshPart(led.UsedArea, led.UsedAreaLines);
		float all = 0;
		for(LedScreenPixCachePoint current : led.PixCache)	{
			all += GetLight(current.X, current.Y) * current.Intensity;
		}
		return (byte) (all / led.PixDivide);
	}
	
	private LedScreenPixCachePoint[] ConvertToArray(Stack<LedScreenPixCachePoint> map)	{
		int l = map.size();
		LedScreenPixCachePoint[] array = new LedScreenPixCachePoint[l];
		int i = 0;
		for(LedScreenPixCachePoint current : map)	{
			array[i++] = current;
		}
		return array;
	}
	
	private float GetLightIntensity(int light, float spaceAway){
		return light / spaceAway;
	}
	
	private int GetCountDirection(int movement, int skip)	{
		return movement < 0 ? -(skip +1) : skip +1;
	}
	
	private int GetLight(int x, int y)	{
		int color = _ssp.GetRGB(x, y);
		return (color & 255) + ((color << 8) & 255) + ((color << 16) & 255);
	}
}
