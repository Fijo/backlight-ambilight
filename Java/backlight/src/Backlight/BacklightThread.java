package Backlight;

import java.awt.AWTException;
import java.io.IOException;

public class BacklightThread {
	private BacklightConfiguration _bc;
	private ComWriter _ac;
	private ScreenWatcher _sw;
	
	public BacklightThread(BacklightConfiguration bc, ComWriter ac) throws AWTException	{
		_bc = bc;
		_ac = ac;
		_sw = new ScreenWatcher(GetScreenShotPix(bc.ScreenFetchType));
		InitLeds();
	}
	
	private IScreenShotPix GetScreenShotPix(ScreenFetchType fetchType) throws AWTException	{
		switch(fetchType)	{
		case PerRefresh:
			return new ScreenShotPrefetchPix();
		case PerLed:
			return new ScreenShotPrefetchPartPix();
		case PerLine:
			return new ScreenShotPrefetchLinePix();
		case PerPix:
			return new ScreenShotPix();
		}
		return null;
	}
	
	private void Process() throws IOException, InterruptedException	{
		FpsWatcher watcher = new FpsWatcher(System.out, "%s Fps", _bc.PrintFpsFrame, _bc.PrintFpsFrame != 0);
		while(true)	{
			BeginUpdateLeds();
			UpdateLeds();
			watcher.FrameEnd();
		}
	}
	
	public void Start()	{
		try {
			Process();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void InitLeds()	{
		Led[] leds = _bc.Leds;
		for(int i = 0, l = leds.length; i < l; i++) _sw.InitLedLight(leds[i]);
		_sw.FinishInit();
	}
	
	public void BeginUpdateLeds() throws InterruptedException	{
		Thread.sleep(_bc.WaitBlock);
		_sw.Refresh();
	}
	
	public void UpdateLeds() throws IOException, InterruptedException	{
		for(int i = 0, l = _bc.Leds.length; i < l; i++)	{
			if(i != 0) WaitLed();
			UpdateLed(_bc.Leds[i]);
		}
	}
	
	private void WaitLed() throws InterruptedException	{
		Thread.sleep(_bc.WaitBetween);
	}
	
	private void UpdateLed(Led current) throws IOException, InterruptedException	{
		_ac.DimLed(current, _sw.GetLedLight(current));
	}
}
